/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: readonly string[] = [
  "/",
  "/login"
];
