import url from "url";

export interface ServerAppConfig {
  externalBaseUri?: url.URL;
}
