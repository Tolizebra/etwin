# 0.0.2 (2020-03-30)

- **[Feature]** Add `addInMemoryUser` method

# 0.0.1 (2020-03-30)

- **[Feature]** First release
