/**
 * Raw HTML content.
 */
export type HtmlText = string;
