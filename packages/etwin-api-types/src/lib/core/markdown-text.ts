/**
 * Raw markdown content.
 */
export type MarkdownText = string;
