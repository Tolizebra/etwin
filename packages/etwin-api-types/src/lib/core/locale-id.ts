/**
 * An ISO locale ID.
 *
 * Pattern: `^[a-z]{2,3}(?:-[A-Z]{1,3})$`
 * Examples: `fr`, `en-US`, `en-GB, `es`
 */
export type LocaleId = string;
