/**
 * UUID as a hex string.
 */
export type UuidHex = string;
