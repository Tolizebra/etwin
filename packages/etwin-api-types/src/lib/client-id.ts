/**
 * The client id is a UUID representing a client application consuming the Eternal-Twin API.
 */
export type ClientId = string;
