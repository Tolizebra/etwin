/**
 * The auth token a secret to perform actions on behalf of a user.
 */
export type AuthToken = string;
