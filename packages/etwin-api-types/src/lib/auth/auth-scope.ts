/**
 * The auth scope allows to precise the scope to use when checking authorizations.
 *
 * For example a user may create a token with restricted privileges.
 */
export enum AuthScope {
  Default,
}
