export enum AuthType {
  /**
   * Unauthenticated
   */
  Guest,

  /**
   * Authenticated as a user.
   */
  User,

  /**
   * System action
   */
  System,
}
