/**
 * The user id is a UUID representing an Eternal-Twin user.
 */
export type UserId = string;
