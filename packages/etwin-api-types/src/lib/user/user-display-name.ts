/**
 * The user display name: the name of the user as it should be displayed on interfaces.
 * It may be non-unique.
 */
export type UserDisplayName = string;
